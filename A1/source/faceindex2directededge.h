#ifndef _FI2DE_HEAD
#define _FI2DE_HEAD
#include <iostream>
#include <vector>

using namespace std;

typedef struct {float x,y,z;} Vert3f;
typedef struct {int v1,v2,v3;} TriFace;
typedef struct {int v1,v2;} Edge2v;
vector<string> fileContents;
int faceCount;
int vertexCount;
vector<Vert3f> verts;
vector<TriFace> faces;

vector<int> firstDE;
vector<Edge2v> edges;
vector<int> oHalf;

vector<bool> vertVisited;

int main (int argc, char *argv[]);
int readFaceFile(string fileName);
void parseHeader();
int parseVerts();
int parseFaces();
void initArrays();
int convertFaces();
int createEdge(int v1, int v2);
void printArrays();
void writeToFile(string fileName);
int checkManifold();
int checkCycles(int vIndex);
int calcGenus(int sCount);
int countShapes();
void visitVert(size_t vIndex);

bool equalEdges(Edge2v a, Edge2v b);

#endif