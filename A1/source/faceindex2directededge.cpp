#include <iostream>
#include <algorithm>
#include <fstream>

#include "faceindex2directededge.h"

using namespace std;

int main (int argc, char *argv[]) {
    if (argc != 2) {
        cout<<"Please supply one argument: \n1: The .face file to be converted\n";
        return 0;
    }

    string fileInName = argv[1];
    if (fileInName.substr(fileInName.length()-5) != ".face") {
        cout << "Please supply an input argument with the .face file extension\n";
        return 0;
    }

    cout << "File to be converted: " << fileInName << "\n";

    if (!readFaceFile(fileInName)) {
        return 0;
    }


    // Parse the info read from the file
    try
    {    
        parseHeader();

        if (!parseVerts()){
            return 0;
        }
        if (!parseFaces()) {
            return 0;
        }
    }
    catch(const invalid_argument& e)
    {
        cout<<"Invalid .face file format"<<endl;
    }
    
    initArrays();
    if (!convertFaces()) {
        return 0;
    }
    // printArrays();
    if (checkManifold()) {
        int sCount = countShapes();
        if (sCount == 1) {
            cout << "Genus of shape: " << calcGenus(1) << endl;
        } else {
            cout << "The file contains: " << sCount << " disconnected shapes." << endl;
            cout << "If all " << sCount << " shapes are identical, they have genus: " << calcGenus(sCount) << endl;
        }
        
    }

    writeToFile(fileInName);
    

}

// Returns whether there is multiple disconnected shapes in the file
int countShapes() {
    int shapeCount = 0;
    for (size_t i = 0; i < vertexCount; i++)
    {
        vertVisited.push_back(false);
    }

    // O(v)
    for (size_t i = 0; i < vertexCount; i++)
    {
        // Recursively visit vert can be called at most once per vertex
        // Therefore O(v)
        if (!vertVisited[i]) {
            shapeCount++;
            visitVert(i);
        }
    }
    
    return shapeCount;
}

// Visits the vertex and recursively visits all connected vertices
void visitVert(size_t vIndex) {
    vertVisited[vIndex] = true;

    for (size_t i = 0; i < edges.size(); i++)
    {
        if (edges[i].v1 == vIndex and !vertVisited[edges[i].v2])
        {
            visitVert(edges[i].v2);
        }
    }
}

// Calculate the genus of a mesh using eulers formula
// formula: v - e + f = 2 - 2g
//          g = (2 - v + e - f) / 2
int calcGenus(int sCount) {
    int v = verts.size() / sCount;
    // Edges is halved as the array stores the set of half edges
    int e = edges.size()/2 / sCount;
    int f = faces.size() / sCount;

    int g = ((2-v)+(e-f))/2;
    return g;
}

void writeToFile(std::string fileName) {
    std::string fileOutName = fileName.substr(0,fileName.length()-5);

    std::ofstream outFile(fileOutName + ".diredge");

    if (outFile.is_open()) {
        // Copy the header lines form the input file
        outFile << "# University of Leeds 2020-2021\n"
            "# COMP 5821M Assignment 1\n"
            "# Ben Hulse\n"
            "# 201125516\n"
            "#\n"
            "# Object Name: " << fileOutName.substr(fileOutName.find_last_of('/')+1)<<"\n"
            "# Vertices="<<verts.size()<<"\n# Faces="<<faceCount<<"\n#\n";

        
        // Write Vertex array
        for (int i = 0; i < vertexCount; i++)
        {
            outFile << "Vertex " << i << " " << verts[i].x << " " << verts[i].y << " " << verts[i].z << std::endl;
        }

        // Write firstDE array
        for (size_t i = 0; i < vertexCount; i++)
        {
            outFile << "FirstDirectedEdge " << i << " " << firstDE[i] << "\n";
        }

        // Write Face Array
        for (int i = 0; i < faceCount; i++) {
            outFile << "Face " << i << " " << faces[i].v1 << " " << faces[i].v2 << " " << faces[i].v3 << std::endl;
        }

        // Write oHalf Array
        for (size_t i = 0; i < faceCount * 3; i++)
        {
            outFile << "OtherHalf " << i << " " << oHalf[i] << "\n";
        }

        std::cout << "Output to file: "<< fileOutName + ".diredge"<<"\n";
        outFile.close();
    } else {
        std::cout<< "File could not be opened\n";
    }
}

void printArrays(){
    cout << "Vertices Array\n";
    for (size_t i = 0; i < vertexCount; i++)
    {
        cout << "Vertex " << i << " " << verts[i].x << " " << verts[i].y << " " << verts[i].z << "\n";
    }
    cout << "Faces Array\n";
    for (size_t i = 0; i < faceCount; i++)
    {
        cout << "Face " << i << " " << faces[i].v1 << " " << faces[i].v2 << " " << faces[i].v3 << "\n";
    }
    cout << "FDE Array\n";
    for (size_t i = 0; i < vertexCount; i++)
    {
        cout << "v" << i << ":e" << firstDE[i] << "\n";
    }
    cout << "Edges Array\n";
    for (size_t i = 0; i < faceCount * 3; i++)
    {
        cout << "Edge " << i << " v" << edges[i].v1  << " v" << edges[i].v2 << "\n";
    }
    cout << "Other Half Array\n";
    for (size_t i = 0; i < faceCount * 3; i++)
    {
        cout << "e" << i << ":e" << oHalf[i] << "\n";
    }
    
}

int convertFaces() {
    // For each face
    for (size_t i = 0; i < faceCount; i++)
    {
        // Get the face being processed
        TriFace face = faces[i];

        if (!createEdge(face.v1,face.v2))
            return 0;
        if (!createEdge(face.v2,face.v3))
            return 0;
        if (!createEdge(face.v3,face.v1))
            return 0;

    }
    return 1;
}

// Create an edge between v1 and v2
int createEdge(int v1, int v2) {
    // Create the edge
    size_t eIndex = (size_t) edges.size();
    edges.push_back(Edge2v{v1, v2});

    // If the first vertex of the edge has no firstDE value, set it to the edge just created
    if (firstDE[v1]==-1) {
        firstDE[v1]=eIndex;
    }

    // Look for an other half for the new edge
    Edge2v desiredOHalf{v2,v1};
    bool found = false;
    for (size_t j = 0; j < edges.size(); j++) {
        if (equalEdges(edges[j],desiredOHalf)) {
            if (oHalf[j] != -1) {
                std::cout<<"Error parsing shape, non manifold surface: three faces on edge: " << eIndex << "\n";
                return 0;
            }
            oHalf[eIndex] = j;
            oHalf[j] = eIndex;
            found = true;
            break;
        }
    }
    return 1;
}

int checkManifold() {
    // Check that every directed edge has a pair edge in the mesh e.g. no holes in the mesh
    for (size_t i = 0; i < faceCount*3; i++)
    {
        if (oHalf[i] == -1) {
            std::cout << "Mesh is not manifold. No Other Half to edge " << i << "\n";
            return 0;
        }
    }

    for (size_t i = 0; i < vertexCount; i++)
    {
        if (!checkCycles(i))
            return 0;
    }

    return 1;
}

int checkCycles(int vIndex) {
    // How many edges are connected to the vertex vIndex
    int conEdgeCount = 0;
    for (size_t i = 0; i < faceCount*3; i++)
        if(edges[i].v1==vIndex)
            conEdgeCount++;
    
    // Until the cycle is finished
    bool finished = false;
    int currEdge = firstDE[vIndex];
    while (!finished) {
        conEdgeCount--;

        int currOH = oHalf[currEdge];
        // Calculate the index of next edge on the same face as currOH
        currEdge = (currOH - (currOH % 3)) + ((currOH+1)%3);

        // If the cycle has returned to the starting edge
        if (currEdge==firstDE[vIndex])
            finished = true;
    }
    if (conEdgeCount != 0) {
        std::cout << "Pinch point detected at vertex: "<<vIndex<<"\n"; 
        return 0;
    }
    return 1;

}


void initArrays() {
    for (size_t i = 0; i < vertexCount; i++)
    {
        firstDE.push_back(-1);
    }
    for (size_t i = 0; i < faceCount*3; i++)
    {
        oHalf.push_back(-1);
    }
}

int parseVerts() {
    for (size_t i = 0; i < vertexCount; i++)
    {
        // Find the index in the fileContents array of the vertex being processed
        size_t currVertIndex = 25 + (i * 5);

        if (fileContents[currVertIndex] != "Vertex"){
            cout<<"File invalid: not enough vertices specified"<<endl;
            return 0;
        }
        // Find the vertex number in the string
        // This isnt used as the verts are stored in ascending order anyway and can be implied
        float x,y,z;
        x = stof(fileContents[currVertIndex+2]);
        y = stof(fileContents[currVertIndex+3]);
        z = stof(fileContents[currVertIndex+4]);

        verts.push_back(Vert3f{x,y,z});
    }
    return 1;
}

int parseFaces() {
    for (size_t i = 0; i < faceCount; i++)
    {
        size_t currFaceIndex = 25 + (5 * vertexCount) + (i * 5);

        if (fileContents[currFaceIndex] != "Face"){
            std::cout<<"File invalid: not enough faces specified" << endl;
            return 0;
        }
        int v1,v2,v3;
        v1 = stof(fileContents[currFaceIndex + 2]);
        v2 = stof(fileContents[currFaceIndex + 3]);
        v3 = stof(fileContents[currFaceIndex + 4]);

        faces.push_back(TriFace{v1,v2,v3});
    }
    return 1;
}

void parseHeader() {
    // Get the vertex and face count from the header
    vertexCount = stoi(fileContents[21].substr(9));
    faceCount = stoi(fileContents[23].substr(6));
}

// Function to read from the supplied file to the fileContents String
// Returns 1 if sucessful, 0 otherwise
int readFaceFile (string fileName) {
    // open the file with the supplied file name
    ifstream file(fileName);
    // If the file is oppened succesfully
    if (file.is_open()){
        
        // Read each word one by one into the fileContents vector
        string token;
        while (file >> token) {
            fileContents.push_back(token);
        }

        file.close();
    } else {
        std::cout<<fileName<<": file not found\n";
        return 0;
    }
    return 1;
}

bool equalEdges(Edge2v a, Edge2v b) {
    return (a.v1==b.v1 and a.v2 == b.v2);
}