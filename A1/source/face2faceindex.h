#ifndef _F2FI_HEAD
#define _F2FI_HEAD
#include <iostream>
#include <vector>

using namespace std;

typedef struct {float x,y,z;} Vert3f;
typedef struct {int v1,v2,v3;} TriFace;
vector<string> fileContents;
int faceCount;
vector<Vert3f> verts;
vector<TriFace> faces;

int main (int argc, char *argv[]);
int readTriFile(string fileName);
void parseFile();
int getVertexIndex(int fcIndex);
void writeToFile(string fileName);
bool equalVertices(Vert3f a, Vert3f b);

#endif