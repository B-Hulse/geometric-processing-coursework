#include <string>
#include <vector>
#include <iostream>
#include <map>
#include "math.h"
#include <algorithm>

using namespace std;

struct Vertex3f {
    float x,y,z;
    bool deleted = false;
    
    Vertex3f()
    {
        x = 0; y = 0; z = 0;
    }

    Vertex3f(float _x, float _y, float _z)
    {
        x = _x; y = _y; z = _z;
    }

    void Normalise() {
        *this /= Length();
    }

    float Length() {
        return sqrt(x*x + y*y + z*z);
    }

    float Length2() {
        return x*x + y*y + z*z;
    }

    float Dot(Vertex3f other) {
        return (x*other.x) + (y*other.y) + (z*other.z);
    }

    Vertex3f operator +(Vertex3f other) {
        return Vertex3f(x+other.x, y+other.y,z+other.z);
    }
    
    Vertex3f operator -(Vertex3f other) {
        return Vertex3f(x-other.x, y-other.y,z-other.z);
    }

    void operator +=(Vertex3f other) {
        x += other.x;
        y += other.y;
        z += other.z;
    }

    Vertex3f operator /(float divisor) {
        return Vertex3f(x/divisor,y/divisor,z/divisor);
    }

    void operator /=(float divisor) {
        x /= divisor;
        y /= divisor;
        z /= divisor;
    }
};

inline std::ostream& operator<<(std::ostream& os, const Vertex3f & v) {
    os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
    return os;
}

struct Face3i {
    size_t v1,v2,v3;
    bool delted = false;

    Face3i(size_t _v1, size_t _v2, size_t _v3) :
    v1(_v1), v2(_v2), v3(_v3) 
    {}
};

struct HalfEdge2i {
    size_t v1,v2;

    bool operator==(HalfEdge2i other) {
        return (v1==other.v1 and v2 == other.v2);
    }
};

class DirEdge {
public: 
    // ----- Object Data -----
    // Vertex Data
    vector<Vertex3f> verts;
    vector<Vertex3f> norms;
    vector<int> firstDE;
    size_t vCount;

    // Face Data
    vector<Face3i> faces;
    size_t fCount;

    // Edge Data
    vector<HalfEdge2i> hEdges;
    vector<int> otherH;
    size_t eCount;

    // ----- Data used for connected component splitting

    size_t index=0;

    // A lookup for the index of a vert or edge
    // The order is <fullMesh, thisMesh>
    map<size_t,size_t> vertLookup;

public:
    DirEdge();
    void printMesh();
    void writeMesh(string fileName);
    bool parseFile(vector<string> fileData);
    void RebuildDecimated(DirEdge *old);
    void GenHEdge(size_t v1, size_t v2);

};