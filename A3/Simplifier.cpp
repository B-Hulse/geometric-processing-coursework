#include "Simplifier.h"
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <algorithm>

using namespace std;

// Constructor
Simplifier::Simplifier() {
}

void Simplifier::separate() {
    visited.assign(fullMesh.vCount,false);
    vertDest.assign(fullMesh.vCount,-1);

    int meshCount = 0;

    for (size_t i = 0; i < fullMesh.vCount; i++)
    {
        if (!visited[i]) {
            DirEdge newMesh;
            newMesh.index = meshCount++;
            visitVert(i,&newMesh);

            splitMesh.push_back(newMesh);
        }
    }
    assignEdgesandFaces();    
}

void Simplifier::assignEdgesandFaces() {
    
    // For each face in the original mesh, add it with the correct vertices to the split mesh
    for (size_t i = 0; i < fullMesh.eCount; i++)
    {
        HalfEdge2i e = fullMesh.hEdges[i];
        DirEdge *mesh = &(splitMesh[vertDest[e.v1]]);

        size_t destV1 = mesh->vertLookup[e.v1];
        size_t destV2 = mesh->vertLookup[e.v2];

        size_t newEIndex = mesh->hEdges.size();

        // Assign first directed edges
        if (mesh->firstDE[destV1] == -1) mesh->firstDE[destV1] = newEIndex;

        mesh->otherH.push_back(-1);
        mesh->hEdges.push_back(HalfEdge2i{destV1,destV2});
    }

    for (size_t i = 0; i < splitMesh.size(); i++)
    {
        DirEdge *mesh = &(splitMesh[i]);
        for (size_t j = 0; j < mesh->hEdges.size(); j++)
        {
            for (size_t k = 0; k < mesh->hEdges.size(); k++)
            {
                if (j==k) continue;
                if (mesh->hEdges[j].v1 == mesh->hEdges[k].v2 and mesh->hEdges[j].v2 == mesh->hEdges[k].v1) {
                    mesh->otherH[j] = k;
                    mesh->otherH[k] = j;
                }
            }
        }
    }
    

    // Repeat for faces
    for (size_t i = 0; i < fullMesh.fCount; i++)
    {
        Face3i f = fullMesh.faces[i];
        DirEdge *mesh = &(splitMesh[vertDest[f.v1]]);

        size_t destV1 = mesh->vertLookup[f.v1];
        size_t destV2 = mesh->vertLookup[f.v2];
        size_t destV3 = mesh->vertLookup[f.v3];

        mesh->faces.push_back(Face3i(destV1,destV2, destV3));
    }
    

}

void Simplifier::visitVert(size_t vIndex, DirEdge *mesh) {
    visited[vIndex] = true;
    vertDest[vIndex] = mesh->index;
    mesh->vertLookup[vIndex] = mesh->verts.size();
    mesh->verts.push_back(fullMesh.verts[vIndex]);
    mesh->norms.push_back(fullMesh.norms[vIndex]);
    mesh->firstDE.push_back(-1);

    for (size_t i = 0; i < fullMesh.eCount; i++)
    {
        if (fullMesh.hEdges[i].v1 == vIndex and !visited[fullMesh.hEdges[i].v2])
        {
            visitVert(fullMesh.hEdges[i].v2, mesh);
        }
    }
}

void Simplifier::simplify(int vRemove) {
    // O(n)
    getORings();
    // O(n)
    fillPQueue();
    // O(1)
    for (size_t i = 0; i < vRemove; i++)
    {
        greedyDecimation();
        cout << "Simplified " << i << endl;
    }
    

    DirEdge newMesh;
    cout << "Rebuilding Mesh" << endl;
    newMesh.RebuildDecimated(&fullMesh);

    fullMesh = newMesh;
}

// // O(N(v)^2)
// void Simplifier::greedyDecimation() {
//     // Vertex being removed
//     int v0 = pQueue.top().second;
//     pQueue.pop();
//     // The first v in v0's one-ring
//     // Get the first face in v0s One-ring
//     Face3i f1 = fullMesh.faces[oRingf[v0][0]];
//     int v1;
//     if (f1.v1 != v0) v1 = f1.v1;
//     else v1 = f1.v2;
//     // TODO: Identify deleted verts
//     fullMesh.verts[v0].deleted = true;
//     // Todo: fix the weighting of the remaining vertices
//
//     // For each face in the one-ring of v0
//     for (size_t i = 0; i < oRingf[v0].size(); i++)
//     {
//         // Get the face in question: fi is index, f is face
//         int fi = oRingf[v0][i];
//         Face3i f = fullMesh.faces[fi];
//
//         // Delete the faces with v1 as a vertex
//         if (f.v1 == v1 or f.v2 == v1 or f.v3 == v1) {
//             f.delted = true;
//
//             // remove f1 from the one-rings of it s vertices
//             oRingf[f.v1].erase(find(oRingf[f.v1].begin(), oRingf[f.v1].end(), fi));
//             oRingf[f.v2].erase(find(oRingf[f.v2].begin(), oRingf[f.v2].end(), fi));
//             oRingf[f.v3].erase(find(oRingf[f.v3].begin(), oRingf[f.v3].end(), fi));
//
//             continue;
//         }
//
//         // Replace v0 in the face with v1
//         if (f.v1 = v0) f.v1 = v1;
//         if (f.v2 = v0) f.v2 = v1;
//         if (f.v3 = v0) f.v3 = v1;
//
//         // Add face to v1s one-ring
//         oRingf[v1].push_back(fi);
//     }
//
//     // Reset priorities
//     fillPQueue();
// }

// O(N(v)^2)
// Move to pointers
void Simplifier::greedyDecimation() {
    // Vertex being removed
    int v0 = pQueue.top().second;
    pQueue.pop();

    // The first v in v0's one-ring
    // Get the first face in v0s One-ring
    Face3i *f1 = &(fullMesh.faces[oRingf[v0][0]]);
    // Set v1 as the index of a vertex in face1 that is not v0
    int v1;
    if (f1->v1 != v0) v1 = f1->v1;
    else v1 = f1->v2;

    fullMesh.verts[v0].deleted = true;

    // For each face in the one-ring of v0
    for (size_t i = 0; i < oRingf[v0].size(); i++)
    {
        // Get the face in question: fi is index, f is face
        int fi = oRingf[v0][i];
        Face3i* f = &(fullMesh.faces[fi]);

        // Delete the faces with v1 as a vertex
        if (f->v1 == v1 or f->v2 == v1 or f->v3 == v1) {
            f->delted = true;
            // cout << " deleted" << endl;

            // remove f1 from the one-rings of it s vertices
            if (f->v1 != v0) oRingf[f->v1].erase(find(oRingf[f->v1].begin(), oRingf[f->v1].end(), fi));
            if (f->v2 != v0) oRingf[f->v2].erase(find(oRingf[f->v2].begin(), oRingf[f->v2].end(), fi));
            if (f->v3 != v0) oRingf[f->v3].erase(find(oRingf[f->v3].begin(), oRingf[f->v3].end(), fi));

            continue;
        }

        // Replace v0 in the face with v1
        if (f->v1 == v0) f->v1 = v1;
        if (f->v2 == v0) f->v2 = v1;
        if (f->v3 == v0) f->v3 = v1;

        // Add face to v1s one-ring
        oRingf[v1].push_back(fi);
    }

    // Reset priorities
    fillPQueue();
}

// Fills the ORings vector
// O(f)
void Simplifier::getORings() {
    oRingf.resize(fullMesh.vCount);
    for (size_t fi = 0; fi < fullMesh.faces.size(); fi++)
    {
        Face3i f = fullMesh.faces[fi];
        // cout << "Face " << fi << " " << f.v1 << " " << f.v2 << " " << f.v3 << endl;
        oRingf[f.v1].push_back(fi);
        oRingf[f.v2].push_back(fi);
        oRingf[f.v3].push_back(fi);
    }
    
}

// O(v+N(v))
void Simplifier::fillPQueue() {    
    // O(nlogn) method of creating a priority queue

    // O(n) method of creating a priority queue
    vector<qPair> v;
    for (size_t i = 0; i < fullMesh.vCount; i++)
    {
        if (!fullMesh.verts[i].deleted) {
            v.push_back(make_pair(vertCurve(i),i));
        }
    }
    pQueue = priority_queue<qPair>(less<qPair>(),v);

}

// Find the area of the vertex i's one ring
float Simplifier::vertArea(int i) {
    
    float areaSum = 0;
    // For each face add area to the sum
    for (size_t j = 0; j < oRingf[i].size(); j++) {
        Face3i f = fullMesh.faces[oRingf[i][j]];
        Vertex3f a,b,c;
        a = fullMesh.verts[f.v1];
        b = fullMesh.verts[f.v2];
        c = fullMesh.verts[f.v3];
        areaSum += triArea(a,b,c);
    }

    return areaSum;
}

// Find the area of the vertex i's one ring
float Simplifier::vertCurve(int i) {
    
    Vertex3f oRingSum = Vertex3f(0.f,0.f,0.f);
    // For each face add area to the sum
    for (size_t j = 0; j < oRingf[i].size(); j++) {
        Face3i f = fullMesh.faces[oRingf[i][j]];
        if (f.v1 != i) oRingSum += fullMesh.verts[f.v1];
        if (f.v2 != i) oRingSum += fullMesh.verts[f.v2];
        if (f.v3 != i) oRingSum += fullMesh.verts[f.v3];
    }

    oRingSum /= oRingf[i].size() * 2;

    Vertex3f diff = fullMesh.verts[i] - oRingSum;

    return diff.Length2();
}

//Find the area of a triangle between three points
float Simplifier::triArea(Vertex3f a, Vertex3f b, Vertex3f c) {
    Vertex3f ab = b-a, ac = c-a;
    float lenABAC = (ab.Length() * ac.Length());
    float cosTheta = ab.Dot(ac) / lenABAC;
    float theta = acos(cosTheta);
    return 0.5f * lenABAC * sin(theta);
}

// Load the file contents to the vector fileData
bool Simplifier::loadMesh(string fileName) {
    ifstream file (fileName.c_str());

    if (!file.is_open()) {
        cout << "Error: Cannot open file" << endl;
        return false;
    }

    string tmp;
    while (file >> tmp) {
        fileData.push_back(tmp);
    }

    if (!fullMesh.parseFile(fileData)) {
        cout << "Error: File is not formatted correctly" << endl;
        file.close();
        return false;
    }

    fileData.clear();
    file.close();
    return true;
}

// Print mesh to console
void Simplifier::printMesh() {
    fullMesh.printMesh();
}

// Write mesh to file
void Simplifier::writeMesh(string fileName) {
    fullMesh.writeMesh(fileName);
}

void Simplifier::writeSplitMesh(string filename) {
    for (size_t i = 0; i < splitMesh.size(); i++)
    {
        string outFile = filename + "_split" + to_string(i) + ".diredgenormal";
        splitMesh[i].writeMesh(outFile);
    }
    
}