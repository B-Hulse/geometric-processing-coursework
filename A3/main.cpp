#include <iostream>
#include <string>
#include "Simplifier.h"

int main(int argc, char const *argv[])
{
    Simplifier s;

    if (argc != 4 and argc != 3) {
        std::cout << "Usage: ./simplifymesh [separate/simplify] [path to .diredgenormal file] [optional: verts to remove]" << endl;
        return 0;
    }

    std::string operation = argv[1];
    std::string file = argv[2];
    int vRemove = 1;
    if (argc == 4) {
        vRemove = stoi(argv[3]);
    }

    if (file.substr(file.length() - 14) != ".diredgenormal") {
        std::cout << "Ensure the file has the type .diredgenormal" << endl;
        return 0;
    }
    
    s.loadMesh(file);
    // s.printMesh();

    if (operation == "separate") {
        s.separate();
        string outFile = file.substr(0,file.length() - 14);
        s.writeSplitMesh(outFile);
    } else if (operation == "simplify") {
        s.simplify(vRemove);
        string outFile = file.substr(0,file.length() - 14) + "_simplified.diredgenormal";
        s.writeMesh(outFile);
    } else {
        std::cout << "Invalid operation specified, must be separate or simplify" << endl;
        return 0;
    }

    return 0;
}