#include "SubDivider.h"
#include <iostream>
#include <fstream>
#include <stdexcept>

using namespace std;

// Constructor
SubDivider::SubDivider() {

}

// Split each edge in two with a new vertex in the middle
void SubDivider::splitEdges() {

    for (size_t i = 0; i < ogECount; i++)
    {        
    
        if (i<otherH[i]) {
            // Find midpoint between edge ends
            Vector3f v1 = verts[hEdges[i].v1], v2 = verts[hEdges[i].v2];
            Vector3f newVert = (v1 + v2) / 2.f;

            verts.push_back(newVert);

            // Repeat for normals
            Vector3f n1 = norms[hEdges[i].v1], n2 = norms[hEdges[i].v2];
            Vector3f newNorm = (n1 + n2) / 2.f;

            norms.push_back(newNorm);
        }
    }
}

// Set the faces and edges with the new vertices
void SubDivider::updateFaces() {
    // Quadruple the size of the vector
    faces.resize(ogFCount * 4);
    newHEdges.resize(faces.size() *3);
    newOtherH.assign(faces.size() *3, -1);
    fHalf.resize(ogECount);
    sHalf.resize(ogECount);
    firstDE.assign(verts.size(),-1);

    for (size_t i = 0; i < ogFCount; i++) {
        // Find the three edge IDs of the face i
        size_t eID1 = edgeID[i*3]; 
        size_t eID2 = edgeID[i*3+1];
        size_t eID3 = edgeID[i*3+2];

        // get the indices of each of the new verts on the old edges of the face i
        size_t eID1V = eID1+ogVCount;
        size_t eID2V = eID2+ogVCount;
        size_t eID3V = eID3+ogVCount;

        // Copy the old vertex indices of the face i
        size_t oldV1 = faces[i].v1;
        size_t oldV2 = faces[i].v2;
        size_t oldV3 = faces[i].v3;

        // Define the new faces
        Face3i newF0{eID1V,eID2V,eID3V};
        Face3i newF1{oldV1,eID2V,eID1V};
        Face3i newF2{oldV2,eID3V,eID2V};
        Face3i newF3{oldV3,eID1V,eID3V};

        // The indecis of the new faces
        size_t f0i = i + (0 * ogFCount);
        size_t f1i = i + (1 * ogFCount);
        size_t f2i = i + (2 * ogFCount);
        size_t f3i = i + (3 * ogFCount);

        // Add faces to face array and define edges

        // Face 1 (i)
        faces[f0i] = newF0;
        // Add the first edge of the middle face
        newHEdges[3*f0i + 0] = HalfEdge2i{eID3V,eID1V};  // v3 -> v1
        // Define the other half of the edge
        // Because we are always defining both halves of the edges that are between the new vertices, we can set their values in the other half array here
        newOtherH[3*f0i + 0] = 3*f3i + 2;
        newOtherH[3*f3i + 2] = 3*f0i + 0;
        // If the first vertex of the half edge doesn't have a firstDE then set it
        if (firstDE[eID3V] == -1) firstDE[eID3V] = 3*f0i+0;

        newHEdges[3*f0i + 1] = HalfEdge2i{eID1V,eID2V};  // v1 -> v2
        newOtherH[3*f0i + 1] = 3*f1i + 2;
        newOtherH[3*f1i + 2] = 3*f0i + 1;
        if (firstDE[eID1V] == -1) firstDE[eID1V] = 3*f0i+1;

        newHEdges[3*f0i + 2] = HalfEdge2i{eID2V,eID3V};  // v2 -> v3
        newOtherH[3*f0i + 2] = 3*f2i + 2;
        newOtherH[3*f2i + 2] = 3*f0i + 2;
        if (firstDE[eID2V] == -1) firstDE[eID2V] = 3*f0i+2;

        // repeate for other faces
        // Face 2 (i + faceCount)
        faces[f1i] = newF1;

        newHEdges[3*f1i+0] = HalfEdge2i{eID1V,oldV1};
        sHalf[i*3+ 0] = 3*f1i+0;

        newHEdges[3*f1i+1] = HalfEdge2i{oldV1,eID2V};
        fHalf[i*3 + 1] = 3*f1i+1;
        if (firstDE[oldV1] == -1) firstDE[oldV1] = 3*f1i+1;

        newHEdges[3*f1i+2] = HalfEdge2i{eID2V,eID1V};

        // Face 3 (i + 2faceCount)
        faces[f2i] = newF2;

        newHEdges[3*f2i+0] = HalfEdge2i{eID2V,oldV2};
        sHalf[i*3 + 1] = 3*f2i+0;

        newHEdges[3*f2i+1] = HalfEdge2i{oldV2,eID3V};
        fHalf[i*3 + 2] = 3*f2i+1;
        if (firstDE[oldV2] == -1) firstDE[oldV2] = 3*f2i+1;

        newHEdges[3*f2i+2] = HalfEdge2i{eID3V,eID2V};

        // Face 4 (i + 3faceCount)
        faces[f3i] = newF3;

        newHEdges[3*f3i+0] = HalfEdge2i{eID3V,oldV3};
        sHalf[i*3 + 2] = 3*f3i+0;

        newHEdges[3*f3i+1] = HalfEdge2i{oldV3,eID1V};
        fHalf[i*3 + 0] = 3*f3i+1;
        if (firstDE[oldV3] == -1) firstDE[oldV3] = 3*f3i+1;

        newHEdges[3*f3i+2] = HalfEdge2i{eID1V,eID3V};
    }
}

// Update the other halves that weren't set when they were created
void SubDivider::updateOH() {
    for (size_t i = 0; i < ogECount; i++) {
        newOtherH[fHalf[i]] = sHalf[otherH[i]];
        newOtherH[sHalf[otherH[i]]] = fHalf[i];
    }
}

// Function to begin the subdivision process
void SubDivider::divide() {
    splitEdges();
    updateFaces();
    updateOH();
}

// Load the file contents to the vector fileData
bool SubDivider::loadMesh(string fileName) {
    ifstream file (fileName.c_str());

    if (!file.is_open()) {
        cout << "Error: Cannot open file" << endl;
        return false;
    }

    string tmp;
    while (file >> tmp) {
        fileData.push_back(tmp);
    }

    if (!parseMesh()) {
        cout << "Error: File is not formatted correctly";
        file.close();
        return false;
    }

    file.close();
    return true;
}

// Parse the contents of the fileData vector
bool SubDivider::parseMesh() {
    size_t currToken = 0;

    // -------- Find Vertex and Face Count --------

    // Go to the index of the vert count line
    for (;fileData[currToken++] != "Surface";);
    
    // Convert the string to int
    ogVCount = stoi(fileData[currToken++].substr(9));
    ogFCount = stoi(fileData[currToken++].substr(6));
    ogECount = 3*ogFCount;

    // -------- Parse Vertices/Normals/FDE --------

    // Go to the first instance of "Vertex"
    for (;fileData[currToken] != "Vertex";currToken++);

    // Parse each vertex and push it to the vector
    for (size_t i = 0; i < ogVCount; i++)
    {
        currToken+=2;
        float x,y,z;

        try
        {
            x = stof(fileData[currToken++]);
            y = stof(fileData[currToken++]);
            z = stof(fileData[currToken++]);
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }

        verts.push_back(Vector3f(x,y,z));
    }

    // Parse each normal and push it to the vector
    for (size_t i = 0; i < ogVCount; i++)
    {
        currToken+=2;
        float x,y,z;

        try
        {
            x = stof(fileData[currToken++]);
            y = stof(fileData[currToken++]);
            z = stof(fileData[currToken++]);
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }

        norms.push_back(Vector3f(x,y,z));
    }

    // Parse each FDE and push it to the vector
    for (size_t i = 0; i < ogVCount; i++)
    {
        currToken+=2;
        float x,y,z;
        try
        {
            firstDE.push_back((size_t)stoi(fileData[currToken++]));
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }
    }

    // --------------- Faces/Edges ---------------

    // Parse each face and push it to the vector
    // Each face has 3 edges
    for (size_t i = 0; i < ogFCount; i++)
    {
        currToken+=2;
        size_t v1,v2,v3;

        try
        {
            v1 = (size_t)stoi(fileData[currToken++]);
            v2 = (size_t)stoi(fileData[currToken++]);
            v3 = (size_t)stoi(fileData[currToken++]);
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }
        faces.push_back(Face3i{v1,v2,v3});

        // Files are defined in edge-to form
        hEdges.push_back(HalfEdge2i{v3,v1});
        hEdges.push_back(HalfEdge2i{v1,v2});
        hEdges.push_back(HalfEdge2i{v2,v3});
    }

    // Parse other Halves
    for (size_t i = 0; i < ogECount; i++)
    {
        currToken+=2;

        try
        {
            otherH.push_back((size_t)stoi(fileData[currToken++]));
        }
        catch(const invalid_argument& e)
        {
            cout << "Error: File not properly formatted" << endl;
        }

    }
    
    size_t edgeIDCounter = 0;
    edgeID.resize(hEdges.size());
    // Fill Edges Array    
    for (size_t i = 0; i < hEdges.size(); i++)
    {
        if (i < otherH[i])
        {
            edgeID[i] = edgeID[otherH[i]] = edgeIDCounter++;
        }
    }
    

    // Clear the data array
    fileData.clear();

    return true;
}

// Print mesh to console
void SubDivider::printMesh() {
    for (size_t i = 0; i < verts.size(); i++)
    {
        cout << "Vertex " << i << ":" << endl;
        cout << "   Position: " << verts[i] << endl;
        cout << "   Normal: " << norms[i] << endl;
        cout << "   FDE: " << firstDE[i] <<endl;
    }

    for (size_t i = 0; i < faces.size(); i++)
    {
        cout << "Face " << i << ":" << endl;
        cout << "   Corners: " << faces[i].v1 << " " << faces[i].v2 << " " << faces[i].v3 << endl;
    }

    for (size_t i = 0; i < newHEdges.size(); i++)
    {
        cout << "Half Edge " << i << ":" << endl;
        cout << "   Ends: " << newHEdges[i].v1 << " " << newHEdges[i].v2 << endl;
        cout << "   Other Half: " << newOtherH[i] << endl;
    }
}

// Write mesh to file
void SubDivider::writeMesh(string fileName) {
    ofstream file (fileName.c_str());

    // Open File
    if (!file.is_open()) {
        cout << "Error: Cannot open file to save" << endl;
        return;
    }

    // Write Header to file
    file << "#" << endl << "# Created for Leeds COMP 5821M Autumn 2020" << endl << "#" << endl << "#" << endl;
    file << "# Surface vertices=" << verts.size() << " faces=" << faces.size() << endl << "#" << endl;

    // Write Vertices
    for (size_t i = 0; i < verts.size(); i++)
    {
        file << "Vertex " << i << " " << verts[i].x << " " << verts[i].y << " " << verts[i].z << endl;
    }
    // Write Normals
    for (size_t i = 0; i < verts.size(); i++)
    {
        file << "Normal " << i << " " << norms[i].x << " " << norms[i].y << " " << norms[i].z << endl;
    }
    // Write FDEs
    for (size_t i = 0; i < firstDE.size(); i++)
    {
        file << "FirstDirectedEdge " << i << " " << firstDE[i]<<endl;
    }
    // Write Faces
    for (size_t i = 0; i < faces.size(); i++)
    {
        file << "Face " << i << " " << faces[i].v1 << " " << faces[i].v2 << " " << faces[i].v3 << endl;
    }
    // Write Other Halves
    for (size_t i = 0; i < newOtherH.size(); i++)
    {
        file << "OtherHalf " << i << " " << newOtherH[i] << endl;
    }
    file.close();
}